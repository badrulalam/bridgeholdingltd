<?php get_header(); ?>
<div class="main-cont">
<div class='container'>
    <!--row-->
    <div class='row homebody'>
        <!--first-coloumn-->
        <div class='home-cat-proj'>
            <?php
            //                wp_list_categories(array('include'=>1,'title_li' => '<h3>','</h3>'));
                            $category_link = get_category_link(1);
            ?>
            <!--                <h2><a href="--><?php //echo $category_link;?><!--">--><?php //echo $categories[0]->name?><!--</a></h2>-->
            <div id="first-demo" class="owl-carousel owl-theme">
                <?php

                query_posts('cat=1');
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="item">
                            <a href="<?php echo esc_url( $category_link ); ?>" class="homecategory">
                                <figure>
                                    <?php
                                    the_post_thumbnail( 'large');

                                    echo'<div class="category handed-over">';
                                        echo'<h3><span><strong>handed over </strong><br/> projects</span></h3>';
                                    echo'</div>';

                                    //echo'<figcaption>';
                                    //echo'<div class="projects Xrw-words Xrw-words-1">';
                                    //the_title('<span>', '</span>');
                                    //echo'</div>';
                                    //echo'</figcaption>';
                                    ?>
                                </figure>
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>

        <!--second-coloumn-->
        <div class='home-cat-proj'>
            <?php
//                            wp_list_categories(array('include'=>2,'title_li' => '<h3>','</h3>'));
                            $category_link = get_category_link(2);
            ?>
            <div id="second-demo" class="owl-carousel owl-theme">
                <?php
                query_posts('cat=2');
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="item">
                            <a href="<?php echo esc_url( $category_link ); ?>">
                                <figure>
                                    <?php
                                    the_post_thumbnail( 'large');

                                    echo'<div class="category">';
                                    echo'<h3> <span><strong>Ongoing </strong><br/> projects</span></h3>';
                                    echo'</div>';

                                    //echo'<figcaption>';
                                    //echo'<div class="projects Xrw-words Xrw-words-1">';
                                    //the_title('<span>', '</span>');
                                    //echo'</div>';
                                    //echo'</figcaption>';
                                    ?>
                                </figure>
                            </a>
                        </div>
                    <?php }
                }
                ?>
            </div>
        </div>

        <!--third-coloumn-->
        <div class='home-cat-proj'>
            <?php
            //                wp_list_categories(array('include'=>3,'title_li' => '<h3>','</h3>'));
                            $category_link = get_category_link(3);
            ?>

            <div id="third-demo" class="owl-carousel owl-theme">
                <?php
                query_posts('cat=3');
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();
                        ?>
                        <div class="item">
                            <a href="<?php echo esc_url( $category_link ); ?>">
                                <figure>
                                    <?php
                                    the_post_thumbnail( 'large');

                                    echo'<div class="category">';
                                    echo'<h3> <span><strong>Upcoming </strong><br/> projects</span></h3>';
                                    echo'</div>';

                                    //echo'<figcaption>';
                                    //echo'<div class="projects Xrw-words Xrw-words-1">';
                                    //the_title('<span>', '</span>');
                                    //echo'</div>';
                                    //echo'</figcaption>';
                                    ?>
                                </figure>
                            </a>
                        </div>
                    <?php }?>
                <?php }
                ?>
            </div>
        </div>
    </div>
</div>
<script>


    jQuery( window ).load(function(){
        var calPad= parseInt(jQuery(document).height()) - 300;
        calPad = calPad -  parseInt(jQuery(".row.homebody").height());
        calPad = calPad /2;
        calPad = calPad+'px';
    jQuery(".row.homebody").css("padding-top",calPad);
    });

    jQuery( window ).resize(function() {
        var calPad= parseInt(jQuery(document).height()) - 300;
        calPad = calPad -  parseInt(jQuery(".row.homebody").height());
        calPad = calPad /2;
        calPad = calPad+'px';
        jQuery(".row.homebody").css("padding-top",calPad);
    });

</script>
</div>
<?php get_footer();?>


