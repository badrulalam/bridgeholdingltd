//jQuery(document).ready(function($)
//$( document ).ready(function($)
jQuery(document).ready(function($){
    console.log( "ready!" );

    var first = $("#first-demo");
    var second = $("#second-demo");
    var third = $("#third-demo");

    first.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
        autoPlay : 6000,
        stopOnHover : true,
        pagination : false,
        touchDrag  : false,
        mouseDrag  : false
    });

    second.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
         autoPlay : 6000,
        stopOnHover : true,
        pagination : false,
        touchDrag  : false,
        mouseDrag  : false
    });

    third.owlCarousel({
        navigation : false,
        singleItem : true,
        transitionStyle : "fade",
        autoPlay : 6000,
        stopOnHover : true,
        pagination : false,
        touchDrag  : false,
        mouseDrag  : false
    });


    $( window ).load(function(){
        var windowheight, bodyheight;
        windowheight=$(window).innerHeight()-1;
        bodyheight=$('#bodyId').height();


        if(bodyheight > windowheight){
            console.log('body container is bigger than window')
            console.log(bodyheight)
            console.log(windowheight)
            $('#footer').css('position','relative')

        }else{
            console.log('body container is less than window')
            console.log(bodyheight)
            console.log(windowheight)
            $('#footer').css('position','fixed')

        };


    });

   $( document ).on("click", ".nav.osc-res-nav.nav-tabs.osc-tabs-left-ul li", function(){
        var windowheight, bodyheight;
        windowheight=$(window).innerHeight()-1;
        bodyheight=$('#bodyId').height();

        if(bodyheight > windowheight){
            console.log('body container is bigger than window')
            console.log(bodyheight)
            console.log(windowheight)
            $('#footer').css('position','relative')

        }else{
            console.log('body container is less than window')
            console.log(bodyheight)
            console.log(windowheight)
            $('#footer').css('position','fixed')

        }
    });

    $(window).scroll(function() {
        if($(window).scrollTop()) {
            console.log('position fixed'),
            $( ".projectheader" ).addClass('header'),
            $( ".projectheader" ).animate(1),
            // $( ".bridgeholdingLogo" ).addClass('bridgeholdingLogostyle'),
            $( ".bridgeholdingLogo img" ).animate({
                position: "absolute",
                top: "4px",
                width: "100px"
                },1)

            $( ".slogan" ).addClass('bridgeholdingslogan'),
            $( ".navbar-right" ).css({'padding':'10'})
            $( ".navbar" ).css({'margin-bottom':'0'})

            /******menu-end******/
            $( ".navbar-collapse.collapse" ).addClass('mjh-navbarCollapse'),
            $( ".projectnam" ).addClass('projectNameplus'),
            $( ".projectnam .projectName" ).addClass('projectNameR'),
            //$( ".osc-res-tab.tabbable.osc-tabs-left div" ).first().addClass('tablists')
            $( "#myScrollspy" ).first().addClass('tablists')
            // $( "#myScrollspy" ).animate({
            //     top: "65px"
            // },1)

            $( "#myScrollspy .customeContainer" ).first().addClass('container')
            $( ".osc-res-tab.tabbable.osc-tabs-left .tablists div" ).first().addClass('tabtitles')



        }else{
            console.log('position relative'),
            $( ".header" ).removeClass('header'),
            $( ".projectheader" ).animate(1),

            // $( ".bridgeholdingLogo" ).removeClass('bridgeholdingLogostyle'),
                $( ".bridgeholdingLogo img" ).animate({
                    position: "relative",
                    top: "0px",
                    width: "185px"
                },1)

            $( ".slogan" ).removeClass('bridgeholdingslogan'),
            $( ".navbar-right" ).removeAttr("style")
            $( ".navbar" ).removeAttr("style")

            /******menu-end******/
            $( ".navbar-collapse.collapse" ).removeClass('mjh-navbarCollapse'),
            $( ".projectnam" ).removeClass('projectNameplus'),
            $( ".projectnam .projectName" ).removeClass('projectNameR'),
            //$( ".osc-res-tab.tabbable.osc-tabs-left div" ).removeClass('tablists')
            $( "#myScrollspy" ).removeClass('tablists')
            // $( "#myScrollspy" ).animate({
            //     top: "135px",
            // },1)


            $( "#myScrollspy .customeContainer" ).removeClass('container')
            $( ".osc-res-tab.tabbable.osc-tabs-left .tablists div" ).removeClass('tabtitles')


        }
    });

    $('#isotope-list').css({height:300, width:'100%','overflow-y':'hidden'}).niceScroll({cursorcolor:"#D62929"});
    //$("#isotope-list").niceScroll({cursorcolor:"#D62929"});



    var winWidth =  $('.container').width();
    $('#myScrollspy').width(winWidth);



    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('a.scrollToTop').fadeIn();
        } else {
            $('a.scrollToTop').fadeOut();
        }
    });
    $("a.scrollToTop[href='#top']").click(function() {
        $("html, body").animate({ scrollTop: 0 },3000);
        return false;
    });
    $("a.scrollToBottom").click(function () {
        $("html, body").animate({scrollTop:$(document).height()},3000);
        return false;
        
    });

    // $('#isotope-list .scrollToRight').click(function(){
    //     $slide.animate({'margin-left': '-=100%'}, 600, function(){
    //         // function after clicked right arrow, doesn't have to be here if there is a simple way of doing it :P
    //     });
    // });
    // $( "#isotope-list" ).scroll(function() {
    //     // $( "span" ).css( "display", "inline" ).fadeOut( "slow" );
    // });

    $(".scrollToRight").click(function () {
        var leftPos = $('#isotope-list').scrollLeft();
        $("#isotope-list").animate({scrollLeft: leftPos + 200}, 800);
    });
    $(".scrollToLeft").click(function () {
        var leftPos = $('#isotope-list').scrollLeft();
        $("#isotope-list").animate({scrollLeft: leftPos - 200}, 800);
    });



});



