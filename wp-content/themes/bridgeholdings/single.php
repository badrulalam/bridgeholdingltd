<?php get_header();?>

    <div class="container">
        <?php if ( have_posts() ) : ?>
            <?php while ( have_posts() ) : the_post(); ?>
                <nav id="myScrollspy">
                    <div class="customeContainer">
                        <?php
                        echo '<h3 class="projectName pull-left">';
                        echo get_the_title();
                        echo '</h3>';
                        ?>
                        <ul class="nav nav-pills">
                            <li class="active"><a href="#home">Home</a></li>
                            <!--<li><a href="#specification">Specification</a></li>
                            <li><a href="#locationMap"> Location Map</a></li>-->
                            <li><a href="#photos"> Photos</a></li>
                            <li><a href="#floorPlans"> Floor Plans</a></li>
                        </ul>
                    </div>
                </nav>
                <div id="singlePageScroll">
                    <?php the_content();?>
                </div>
                <a class="scrollToTop" href="#top">
                    <span class="glyphicon glyphicon-menu-up" aria-hidden="true"></span>
                </a>
                <a class="scrollToBottom" href="#bottom">
                    <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span>
                </a>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

<?php get_footer();?>