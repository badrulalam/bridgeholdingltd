<?php get_header(); ?>

    <section class="mjh_thumbnailImg">
        <div class="container-fluid">
            <?php if ( has_post_thumbnail() ) : ?>
                <figure class="mjh_aboutCompany">
                    <h3><?php the_title_attribute(); ?><br/><span> </span></h3>
                    <?php the_post_thumbnail('full'); ?>
                </figure>
            <?php endif; ?>
        </div>
    </section>

    <div class="container">
        <?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();
                    ?>
                    <?php echo do_shortcode(''); ?>
                    <?php
                    the_content();

                endwhile;
            endif;
        ?>
    </div>

 <?php get_footer(); ?>