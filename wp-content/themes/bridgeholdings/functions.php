<?php


/*
 * Enable support for Post Thumbnails on posts and pages.
 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
 */
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 825, 510, true );



/*=========Enable-Nevigation-Bar===========*/
function register_my_menus() {
    register_nav_menus(
        array(
            'new-menu' => __( 'main_navigation' ),
            'another-menu' => __( 'Another Menu' ),
            'an-extra-menu' => __( 'An Extra Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );



/*=========custom-header===========*/
$args = array(
    'width'         => 280,
    'height'        => 60,
    'uploads'       => true,
);
add_theme_support( 'custom-header', $args );





/*=========css/js-files-add===========*/

function ap_core_load_scripts(){
    // Register and Enqueue a Stylesheet
    // get_template_directory_uri will look up parent theme location
    wp_register_style( 'Bootstrap', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap.css','3.3.6');
    wp_enqueue_style( 'Bootstrap' );

    wp_register_style( 'bootstrap-theme', get_template_directory_uri() . '/assets/bootstrap/css/bootstrap-theme.css','3.3.6');
    wp_enqueue_style( 'bootstrap-theme' );

    wp_register_style( 'owl.carousel.css', get_template_directory_uri() . '/plugins/owlcarousel/owl-carousel/owl.carousel.css','1.3.3');
    wp_enqueue_style( 'owl.carousel.css' );
    
    wp_register_style( 'owl.theme.css', get_template_directory_uri() . '/plugins/owlcarousel/owl-carousel/owl.theme.css','1.3.3');
    wp_enqueue_style( 'owl.theme.css' );

    wp_register_style( 'owl.transitions.css', get_template_directory_uri() . '/plugins/owlcarousel/owl-carousel/owl.transitions.css','1.3.3');
    wp_enqueue_style( 'owl.transitions.css' );

    wp_register_style( 'isotope-css', get_stylesheet_directory_uri() . '/assets/css/isotope.css' );
    wp_enqueue_style('isotope-css');

    wp_register_style( 'fancybox.css', get_stylesheet_directory_uri() . '/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5' );
    wp_enqueue_style('fancybox.css');

    /******************fonts*******************/
    wp_register_style( 'pristina', get_stylesheet_directory_uri() . '/assets/fonts/pristina/stylesheet.css' );
    wp_enqueue_style('pristina');

    wp_register_style( 'tektonPro', get_stylesheet_directory_uri() . '/assets/fonts/tektonPro-bold/stylesheet.css' );
    wp_enqueue_style('tektonPro');

    wp_register_style( 'MyriadProBold', get_stylesheet_directory_uri() . '/assets/fonts/MyriadProBold/stylesheet.css' );
    wp_enqueue_style('MyriadProBold');

    wp_register_style( 'Oswald', 'https://fonts.googleapis.com/css?family=Oswald:400,700,300' );
    wp_enqueue_style('Oswald');

    wp_register_style( 'letter-gothic',  get_stylesheet_directory_uri() . '/assets/fonts/letter-gothic/stylesheet.css' );
    wp_enqueue_style('letter-gothic');
    
    wp_register_style( 'style.css', get_template_directory_uri() . '/assets/css/custom.css',array(), time());
    wp_enqueue_style( 'style.css' );





    // Register and Enqueue a Script
    // get_stylesheet_directory_uri will look up child theme location
//    wp_register_script( 'min.js', get_stylesheet_directory_uri() . '/assets/js/jquery-1.9.1.min.js', array('jquery'));
//    wp_enqueue_script( 'min.js' );

    wp_register_script( 'bootstrap.js', get_stylesheet_directory_uri() . '/assets/bootstrap/js/bootstrap.js', array('jquery'));
    wp_enqueue_script( 'bootstrap.js' );

    wp_register_script( 'owl.carousel.js', get_stylesheet_directory_uri() . '/plugins/owlcarousel/owl-carousel/owl.carousel.js', array('jquery'));
    wp_enqueue_script( 'owl.carousel.js' );

    wp_register_script( 'isotope', get_template_directory_uri().'/assets/js/jquery.isotope.min.js', array('jquery'),  true );
    wp_register_script( 'isotope-init', get_template_directory_uri().'/assets/js/isotope.js', array('jquery', 'isotope'),  true );
    wp_enqueue_script('isotope-init');


//    wp_register_script( 'mousewheel.fancybox.js', get_stylesheet_directory_uri() . '/plugins/fancybox/lib/jquery.mousewheel-3.0.6.pack.js', array('jquery'));
//    wp_enqueue_script( 'mousewheel.fancybox.js' );

    wp_register_script( 'masonry-horizontal.js', get_stylesheet_directory_uri() . '/assets/js/masonry-horizontal.js', array('jquery'));
    wp_enqueue_script( 'masonry-horizontal.js' );


    wp_register_script( 'jquery.nicescroll.js', get_stylesheet_directory_uri() . '/plugins/jquery.nicescroll-master/jquery.nicescroll.js', array('jquery'));
    wp_enqueue_script( 'jquery.nicescroll.js' );


    wp_register_script( 'fancybox.js', get_stylesheet_directory_uri() . '/plugins/fancybox/source/jquery.fancybox.pack.js?v=2.1.5', array('jquery'));
    wp_enqueue_script( 'fancybox.js' );

    wp_register_script( 'scrollspy.js', get_stylesheet_directory_uri() . '/assets/js/scrollspy.js', array('jquery'));
    wp_enqueue_script( 'scrollspy.js' );


    wp_register_script( 'custom-script', get_stylesheet_directory_uri() . '/assets/js/custom.js', array('jquery'));
    wp_enqueue_script( 'custom-script' );



}
add_action('wp_enqueue_scripts', 'ap_core_load_scripts');














?>