<?php
/**
 * The template for displaying the header
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> >
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Le styles -->
	<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
	<?php wp_head(); ?>
</head>

<body id="bodyId" data-spy="scroll" data-target="#myScrollspy" data-offset="50">
    <header class="projectheader">
        <div class="container">
            <nav class="navbar navbar-default transparent">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar" aria-expanded="false">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--logo-->
                    <div class="bridgeholdingLogo">
                        <a href="<?php bloginfo('url'); ?>/"><img src="<?php echo( get_header_image() ); ?>" alt="<?php echo( get_bloginfo( 'title' ) ); ?>" />
                            <span class="slogan">redefining lifestyle</span>
                        </a>
                    </div>
                </div>
                <!--navigation-->
                <div class="navbar-collapse collapse" id="myNavbar" aria-expanded="false" style="height: 1px;">
                    <div class="">
                        <?php wp_nav_menu( array( 'theme_location' => 'new-menu', 'container_class' => 'nav navbar-nav navbar-right' ) ); ?>
                    </div>
                </div>
            </nav>
        </div>
    </header>

