<?php get_header();?>
      <div class="container">
		<div class="mjh-catagory">
			<div class="catWiseProjects">
				<ul id="filters">
					<li><a href="#" data-filter="*" class="selected">All Types</a></li>
					<?php
					$terms = get_terms("post_tag"); // get all categories, but you can use any taxonomy
					$count = count($terms); //How many are they?
					if ( $count > 0 ){  //If there are more than 0 terms
						foreach ( $terms as $term ) {  //for each term:
							echo "<li><a href='#' data-filter='.".$term->slug."'>" . $term->name . "</a></li>\n";
							//create a list item with the current term slug for sorting, and name for label
						}
					}
					?>
				</ul>
				<div class="projectsCatagory">
                    <div class="currentCategory"><?php echo single_cat_title( '', false );?></div>
                    <div class="categoryDropdown">
                        <ul>
                        <?php


                        $defaults = array(
                            'child_of'            => 0,
                            'current_category'    => 0,
                            'depth'               => 0,
                            'echo'                => 1,
                            'exclude'             => '',
                            'exclude_tree'        => '',
                            'feed'                => '',
                            'feed_image'          => '',
                            'feed_type'           => '',
                            'hide_empty'          => 1,
                            'hide_title_if_empty' => false,
                            'hierarchical'        => true,
                            'order'               => 'ASC',
                            'orderby'             => 'name',
                            'separator'           => '<br />',
                            'show_count'          => 0,
                            'show_option_all'     => '',
                            'show_option_none'    => __( 'No categories' ),
                            'style'               => 'list',
                            'taxonomy'            => 'category',
                            'title_li'            => __( '' ),
                            'use_desc_for_title'  => 1,
                        );

                        print_r(wp_list_categories($defaults));
                        //                    wp_list_categories();

                        ?>
                        </ul>
                    </div>
                    </div>

			</div>
			<?php if ( have_posts() ){
				?>
				<div id="isotope-list">
					<?php
					while ( have_posts()) {
						the_post();
						$termsArray = get_the_terms( $post->ID, "post_tag" );  //Get the terms for this particular item
						$termsString = ""; //initialize the string that will contain the terms
						if(is_array($termsArray)){
							foreach ( $termsArray as $term ) { // for each term
								$termsString .= $term->slug.' '; //create a string that has all the slugs
							};
						}
						?>
							<div class="<?php echo $termsString; ?> item isotopeitem">
								<div class="singleItems">
									<a href="<?php the_permalink(); ?>" >
										<?php
											if ( has_post_thumbnail() ) {
												the_post_thumbnail( 'medium');
											};
											?>
										<?php
										the_title( '<h6 class="projectNameCatwise">', '</h6>' );
										?>
										</a>

								</div>
							</div>
						<?php
					}
					?>
				</div>
				<a class="scrollToRight">
					<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
				</a>
				<a class="scrollToLeft">
					<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
				</a>
			<?php } ?>
		</div>
      </div><!--container-->

<?php get_footer(); ?>




































