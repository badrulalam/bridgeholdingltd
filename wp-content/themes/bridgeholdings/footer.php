<?php
/**
 * The template for displaying the footer
 * Contains the closing of the #content div and all content after
 * @package WordPress
 * @Bridgeholdings
 *
 */
?>
</div><!-- .site-content -->

<footer id="footer">
    <div class="container t-name">
        <h1>
            <span class="black">Bridge</span> <span class="red">Holdings</span>
            <span class="gray">Ltd.</span>
        </h1>
    </div>

    <div class="footer">
        <div class="">
            <div class="footerpone">

            </div>
            <div class="footertow">
                <h4>Sales Inquiry</h4>
                <h3>+88 09666 77 88 55</h3>
            </div>
            <div class="footerthree">
 		<h3>Powered by <a href="http://www.everexpert.com" target="_blank">Everexpert</a></h3>
            </div>
        </div>

      
    </div> <!-- /container -->
</footer>

<?php wp_footer(); ?>
  </body>
</html>